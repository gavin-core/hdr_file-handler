/*******************************************************/
// This need to be documented somewhere, but for now,
// we'll specify here:
//
// 'in' data: {
//    content: 'string'
//      path: 'string' // full path with filename
//      overwriteExisting: boolean // default false
//  }
// 'out' data: {
//  }
//
/*******************************************************/

module.exports = (data, cb, Discovery) => {
  const core = require('node-core')
  const fs = require('fs')

  core.pathUtils.ensureExists(data.path, (err, dirPath) => {
    if (err) {
      return cb(err)
    }

    fs.stat(data.path, (err, stat) => {
      if (err && err.code !== 'ENOENT') {
        return cb(err)
      }

      if (stat && !data.overwriteExisting) {
        return cb(new Error(`A file already exists at ${data.path}`))
      }

      fs.writeFile(data.path, data.content, cb)
    })
  })
}
