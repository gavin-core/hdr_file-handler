/*******************************************************/
// This need to be documented somewhere, but for now,
// we'll specify here:
//
// 'in' data: {
//    path: 'string' //full path with filename
//      deleteIfNotExists: bool // {default: false}
//  }
// 'out' data: {
//      extension: 'string', //ie jpg
//      name: 'string' // ie AV_0000532345
//      fullName: 'string' // ie AV_0000532345.jpg
//      path: 'string' // C:\\Files
//      fullPath: 'string' // C:\\Files\\AV_0000532345.jpg
//      size: number // in bytes
//  }
//
/*******************************************************/

module.exports = (data, cb, Discovery) => {
  const fs = require('fs')
  const path = require('path')
  const extension = path.extname(data.path)

  fs.stat(data.path, (err, stat) => {
    if (err) {
      return cb({
        message: `File not found at: ${data.path}`,
        handleType: data.deleteIfNotExists ? 'delete' : null
      })
    }

    cb(null, {
      extension: extension.replace(/\./, ''),
      name: path.basename(data.path, extension),
      fullName: path.basename(data.path),
      path: path.dirname(data.path),
      fullPath: data.path,
      size: stat.size
    })
  })
}
