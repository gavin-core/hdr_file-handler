/*******************************************************/
// This need to be documented somewhere, but for now,
// we'll specify here:
//
// 'in' data: {
//    path: 'string' // full path with filename
//    errorIfNotExist: boolean // default false
//  }
// 'out' data: {
//  }
//
/*******************************************************/

module.exports = (data, cb, Discovery) => {
  const fs = require('fs')

  fs.stat(data.path, err => {
    if (err && data.errorIfNotExist) {
      return cb(err)
    }

    fs.unlink(data.path, (err) => {
      if (err) {
        return cb(err)
      }

      return cb()
    })
  })
}
