'use strict'

/*******************************************************/
// This need to be documented somewhere, but for now,
// we'll specify here:
//
// 'in' data: {
//      path: 'string' // path to file
//  }
// 'out' data: {
//  }
//
/*******************************************************/

module.exports = (data, cb, Discovery) => {
  const FileVerifier = require('./file-verifier')
  const fileVerifier = new FileVerifier(data.path)

  fileVerifier.exists(exists => {
    if (!exists) {
      return cb({
        handleType: 'delete',
        message: 'File does not exist'
      })
    }

    fileVerifier.verifyFileExtension(err => {
      if (err) {
        return cb(err)
      }

      fileVerifier.verifyFileIntegrity(err => {
        cb(err)
      })
    })
  })
}
