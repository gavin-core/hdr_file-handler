const fs = require('fs')

const processors = {
  jpg: {
    type: 'StartAndEnd',
    start: [0xFF, 0xD8],
    end: [0xFF, 0xD9]
  },
  png: {
    type: 'StartAndEnd',
    start: [0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A],
    end: [0x49, 0x45, 0x4E, 0x44, 0xAE, 0x42, 0x60, 0x82]
  },
  pdf: {
    type: 'StartAndEnd',
    start: [0x25, 0x50, 0x44, 0x46],
    end: {
      0: [0x0A, 0x25, 0x25, 0x45, 0xAF, 0x46],
      1: [0x0A, 0x25, 0x25, 0x45, 0xAF, 0x46, 0x0A],
      2: [0x0D, 0x0A, 0x25, 0x25, 0x45, 0x4F, 0x46, 0x0D, 0x0A],
      3: [0x0D, 0x25, 0x25, 0x45, 0xAF, 0x46, 0x0D]
    }
  },
  xmp: {
    type: 'StartAndEnd',
    start: [0x3C, 0x3F, 0x78, 0x70, 0x61, 0x63, 0x6B, 0x65, 0x74, 0x20],
    end: [0x3C, 0x3F, 0x78, 0x70, 0x61, 0x63, 0x6B, 0x65, 0x74, 0x20, 0x65, 0x6E, 0x64, 0x3D, 0x22, 0x77, 0x22, 0x3F, 0x3E]
  }

}

const process = (filename, extension, cb) => {
  [
    function getProcessor (next, data) {
      data.extension = extension.toLowerCase()

      if (!(extension in processors)) {
        return next({
          handleType: 'delete',
          message: 'Invalid format'
        })
      }

      next()
    },
    function getFileStat (next, data) {
      fs.stat(filename, (err, stat) => {
        if (err) {
          return next(err)
        }

        data.stat = stat
        next()
      })
    },
    function processFile (next, data) {
      const processor = processors[data.extension]

      switch (processor.type) {
        case 'StartAndEnd':
          let nameParts = filename.split('/')
          let name = nameParts[nameParts.length - 1]

          const _handle = (start, end, cb) => {
            const startStream = fs.createReadStream(filename, { start: 0, end: start.length - 1 })
            const endStream = fs.createReadStream(filename, { start: data.stat.size - end.length, end: data.stat.size - 1 })
            let iterations = 0

            let onComplete = valid => {
              iterations++

              if (!valid) {
                onComplete = function () {}
                return cb(`${name} is NOT a valid ${extension.toUpperCase()}`)
              }

              if (iterations === 2) {
                return cb()
              }
            }

            startStream.on('readable', () => {
              const buffer = startStream.read()

              if (!buffer) {
                return
              }

              for (let i = 0; i < buffer.length; i++) {
                if (buffer[i] !== start[i]) {
                  return onComplete(false)
                }
              }

              onComplete(true)
            })

            endStream.on('readable', () => {
              const buffer = endStream.read()

              if (!buffer) {
                return
              }

              for (let i = 0; i < buffer.length; i++) {
                if (buffer[i] !== end[i]) {
                  return onComplete(false)
                }
              }

              onComplete(true)
            })
          }

          if (processor.end instanceof Array) {
            return _handle(processor.start, processor.end, err => {
              if (err) {
                return next({
                  handleType: 'delete',
                  message: `${name} is NOT a valid ${extension.toUpperCase()}`
                })
              }

              next()
            })
          }

          ;(function attemptToHandle (index) {
            if (index in processor.end && processor.end.hasOwnProperty(index)) {
              return _handle(processor.start, processor.end[index], err => {
                if (err) {
                  return attemptToHandle(++index)
                }

                next()
              })
            }

            return next({
              handleType: 'delete',
              message: `${name} is NOT a valid ${extension.toUpperCase()}`
            })
          })(0)

          break

        case 'StartAndFilesize':
          const startStream = fs.createReadStream(filename, { start: 0, end: processor.start.length - 1 })
          const filesizeStream = fs.createReadStream(filename, { start: processor.filesize.start, end: processor.filesize.end })
          let iterations = 0
          nameParts = filename.split('/')
          name = nameParts[nameParts.length - 1]

          let onComplete = valid => {
            iterations++

            if (!valid) {
              onComplete = function () {}
              return next({
                handleType: 'delete',
                message: `${name} is NOT a valid ${extension.toUpperCase()}`
              })
            }

            if (iterations === 2) {
              return next()
            }
          }

          startStream.on('readable', () => {
            const buffer = startStream.read()

            if (!buffer) {
              return
            }

            for (let i = 0; i < buffer.length; i++) {
              if (buffer[i] !== processor.start[i]) {
                return onComplete(false)
              }
            }

            onComplete(true)
          })

          filesizeStream.on('readable', () => {
            const buffer = filesizeStream.read()

            if (!buffer) {
              return
            }

            for (let i = 0; i < buffer.length; i++) {
              if (buffer[i] !== processor.end[i]) {
                return onComplete(false)
              }
            }

            onComplete(true)
          })
          break

        default:
          return next({
            handleType: 'delete',
            message: `Unsupported processor type: ${processor.type}`
          })
      }
    }
  ].callInSequence(cb || function () {})
}

const FileVerifier = function (path) {
  const _this = this
  const _path = path
  let _extension

  if (_path.lastIndexOf('.')) {
    const parts = _path.split('.')
    _extension = parts[parts.length - 1]
  }

  _this.exists = cb => {
    fs.stat(_path, err => cb(!err))
  }

  _this.verifyFileExtension = cb => {
    cb = cb || function () {}

    if (!(_extension.toLowerCase() in processors)) {
      return cb({
        handleType: 'delete',
        message: 'Unsupported file extension'
      })
    }

    cb()
  }

  _this.verifyFileIntegrity = cb => {
    process(_path, _extension, cb || function () {})
  }

  return _this
}

module.exports = FileVerifier
