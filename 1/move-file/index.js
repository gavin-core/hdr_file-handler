/*******************************************************/
// This need to be documented somewhere, but for now,
// we'll specify here:
//
// 'in' data: {
//    source: 'string' //full path with filename
//      destination: 'string' // full path with filename
//      overwriteExisitng: boolean // default false
//  }
// 'out' data: {
//  }
//
/*******************************************************/

module.exports = (data, cb, Discovery) => {
  const core = require('node-core')
  const fs = require('fs')

  fs.stat(data.source, err => {
    if (err) {
      return cb({
        handleType: 'delete',
        message: `No file found at ${data.source}`
      })
    }

    core.pathUtils.ensureExists(data.destination, (err, dirPath) => {
      if (err) {
        return cb(err)
      }

      const writeStream = fs.createWriteStream(data.destination)
      const readStream = fs.createReadStream(data.source)

      readStream.on('err', err => cb(err))
      readStream.on('end', () => fs.unlink(data.source, cb))

      readStream.pipe(writeStream)
    })
  })
}
