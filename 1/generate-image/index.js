/*******************************************************/
// This need to be documented somewhere, but for now,
// we'll specify here:
//
// 'in' data: {
//    source: 'string' //full path with filename
//    destination: 'string' // full path with filename
//    overwriteExisting: boolean // default true
//  }
// 'out' data: {
//  }
//
/*******************************************************/

module.exports = function (data, cb, Discovery) {
//  console.log(data)
//  return cb(new Error('fack'))

  const core = require('node-core')
  const fs = require('fs')
  const gm = require('gm')

  fs.stat(data.source, err => {
    if (err) {
      return cb(new Error('Source file not found'))
    }

    core.pathUtils.ensureExists(data.destination, err => {
      if (err) {
        return cb(err)
      }

      fs.stat(data.destination, err => {
        if (!err && [0, '0', false, 'false'].includes(data.overwriteExisting)) {
          return cb(new Error(`File exists at ${data.destination} and config doesnt allow overwrite`))
        }

        const gmFile = gm(data.source)

        if (data.config.resize) {
          gmFile.resize(data.config.resize.width, data.config.resize.height)
        }

        if (data.config.sepia) {
          gmFile.sepia()
        }

        if (data.config.compression) {
          gmFile.quality(data.config.compression)
        }

        if (data.config.density) {
          gmFile.density(data.config.density.width, data.config.density.height)
        }

        if (data.config.colorspace) {
          gmFile.colorspace(data.config.colorspace)
        }

        switch (data.config.type) {
          case 'box':
            gmFile.box('white')
            break
        }

        gmFile.write(data.destination, cb)
      })
    })
  })
}
