const queue = require('node-queue')

const tasksToHandle = [
  'deleteFile@1',
  'generateImage@1',
  'getFileInfo@1',
  'moveFile@1',
  'verifyFileIntegrity@1',
  'writeFile@1'
]

const handlers = +process.argv[2] || 5
new queue.Handler(tasksToHandle, handlers)
